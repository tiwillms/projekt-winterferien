package Winterprojekt;

import Winterprojekt.ChainofResponsibility.EuroinPfund;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class FactoryTest {
    InewUmrechner wrEuroin;

    @Before
    public void start(){
        wrEuroin = new WREuroin();
    }

    @Test
    public void neuenUmrechnerhinzufuegen(){

        wrEuroin.umrechnerHinzufügen("EuroinPfund");

        double umgerechneterbetrag = wrEuroin.umrechnen("EuroinPfund", 10);
        assertEquals(8.8, umgerechneterbetrag);
    }
}
