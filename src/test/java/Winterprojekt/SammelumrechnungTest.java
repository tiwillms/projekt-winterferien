package Winterprojekt;

import Winterprojekt.Adapter.ISammelumrechnungsadapter;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class SammelumrechnungTest {
    IUmrechnen umrechner;
    ISammelumrechnung sammelumrechnung;

    @Before
    public void start(){
        this.umrechner = new WREuroin();
        this.sammelumrechnung = new ISammelumrechnungsadapter(umrechner);
    }

    @Test
    public void SammelumrechnungEurozuDollarTest(){
        double[] array = {10.0, 20.0, 30.0, 15.0};
        double ergebnis;

        ergebnis = sammelumrechnung.sammelumrechnung( array , "EuroinDollar");
        assertEquals(85.8, ergebnis);
    }

    @Test
    public void SammelumrechnenEuroinYenTest(){
        double[] array = {10.0, 20.0, 30.0, 15.0};
        double ergebnis;

        ergebnis = sammelumrechnung.sammelumrechnung(array , "EuroinYen" );
        assertEquals(8514.449999999999, ergebnis);
    }
}
