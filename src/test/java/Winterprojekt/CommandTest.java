package Winterprojekt;

import Winterprojekt.Command.FertigeUmrechnung;
import Winterprojekt.Command.Umrechnung;
import Winterprojekt.Command.Umrechnungscommand;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;

public class CommandTest {
    IUmrechnen wrEuroin;
    Umrechnungscommand umrechnungscommand;
    EntityManagerFactory emf;
    EntityManager em;

    @Before
    public void start(){
        //Umrechner
        wrEuroin = new WREuroin();

        //Umrechnungscommand EuroinYen
        umrechnungscommand = new Umrechnung(wrEuroin);
        //WR WRzuEuro = new WRzuEuro();

        emf = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        em = emf.createEntityManager();
    }

    @Test
    public void umrechnenEuroinDollarTest(){

        double umgerechneterbetrag = umrechnungscommand.execute("EuroinDollar", 10);

        //Speichern der Umrechnung in der Mysql DB
        umrechnungscommand.savetoDB(em);

        emf.close();

        assertEquals(11.44, umgerechneterbetrag);
    }

    @Test
    public void umrechnenEuroinYenTest(){

        double umgerechneterbetrag = umrechnungscommand.execute("EuroinYen", 10);
        assertEquals(1135.26, umgerechneterbetrag);
    }

}
