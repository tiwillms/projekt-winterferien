package Winterprojekt;

import Winterprojekt.ChainofResponsibility.AbstractUmrechner;
import Winterprojekt.ChainofResponsibility.EuroinDollarUmrechner;
import Winterprojekt.ChainofResponsibility.EuroinYenUmrechner;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.*;


public class WRTest {
    IUmrechnen wrEuroin;
    //WR wrzuEuro;

    @Before
    public void start(){
        wrEuroin = new WREuroin();
        //WR WRzuEuro = new WRzuEuro();
    }


    @Test
    public void umrechnenEuroinDollarTest(){

        double umgerechneterbetrag = wrEuroin.umrechnen("EuroinDollar", 100);
        assertEquals(114, Math.round(umgerechneterbetrag));
    }

    @Test
    public void umrechnenEuroinYenTest(){

        double umgerechneterbetrag = wrEuroin.umrechnen("EuroinYen", 100);
        assertEquals(11352.6, umgerechneterbetrag);
    }
}
