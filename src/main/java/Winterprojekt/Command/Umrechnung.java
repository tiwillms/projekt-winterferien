package Winterprojekt.Command;

import Winterprojekt.IUmrechnen;

import javax.persistence.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Stack;

public class Umrechnung implements Umrechnungscommand {

    private ArrayList<FertigeUmrechnung> fertigeUmrechnungen = new ArrayList<>();
    private IUmrechnen umrechner;

    public Umrechnung(IUmrechnen newumrechner){
        this.umrechner = newumrechner;
    }

    @Override
    public double execute(String variante, double betrag) {
        //Speichern der Werte damit diese Gelogt werden können
        double ergebnis = this.umrechner.umrechnen(variante, betrag);

        //Fertige Umrechnung wird erstellt und dann in die Datenbank gespeichert
        FertigeUmrechnung f = new FertigeUmrechnung();
        f.setVariante(variante);
        f.setBetrag(betrag);
        f.setErgebnis(ergebnis);

        fertigeUmrechnungen.add(f);

        return ergebnis;
    }

    public void savetoDB(EntityManager em){
        //Starten der Transaktion
        em.getTransaction().begin();

        for (FertigeUmrechnung f : gibFertigeUmrechnungen()) {
            em.persist(f);
        }
        //Speichern in die Datenbank
        em.getTransaction().commit();

        //Schließen der EM
        em.close();
    }

    public ArrayList<FertigeUmrechnung> gibFertigeUmrechnungen(){
        return this.fertigeUmrechnungen;
    }

}
