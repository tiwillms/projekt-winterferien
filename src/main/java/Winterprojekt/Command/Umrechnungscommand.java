package Winterprojekt.Command;

import javax.persistence.EntityManager;

public interface Umrechnungscommand {
    double execute(String variante, double betrag);
    void savetoDB(EntityManager em);
}
