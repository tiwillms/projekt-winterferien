package Winterprojekt.Command;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
public class FertigeUmrechnung {

    @Id
    @GeneratedValue(generator = "incrementor")
    @GenericGenerator(name = "incrementator", strategy = "increment")
    private int id;

    private String variante;
    private double betrag = 0.0;
    private double ergebnis = 0.0;


    public FertigeUmrechnung(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBetrag() {
        return betrag;
    }

    public void setBetrag(double betrag) {
        this.betrag = betrag;
    }

    public double getErgebnis() {
        return ergebnis;
    }

    public void setErgebnis(double ergebnis) {
        this.ergebnis = ergebnis;
    }

    public String getVariante() {
        return variante;
    }

    public void setVariante(String variante) {
        this.variante = variante;
    }

    public double rueckgaengig(){
        return this.betrag;
    }
}
