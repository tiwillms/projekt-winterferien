package Winterprojekt;

import Winterprojekt.ChainofResponsibility.AbstractUmrechner;
import Winterprojekt.ChainofResponsibility.Ergebnis;

public class WREuroin extends WR {

    boolean zuEuro(){return false;}

    @Override
    double umrechneninEuro(AbstractUmrechner umrechnerkette, String variante, double betrag, Ergebnis erg) {
        umrechnerkette.umrechnen(variante, betrag, erg);
        return erg.getWert();
    }
}
