package Winterprojekt;

import Winterprojekt.ChainofResponsibility.AbstractUmrechner;
import Winterprojekt.ChainofResponsibility.Decorator.Logger;
import Winterprojekt.ChainofResponsibility.Decorator.Zusatzgebuehren;
import Winterprojekt.ChainofResponsibility.Ergebnis;
import Winterprojekt.ChainofResponsibility.EuroinDollarUmrechner;
import Winterprojekt.ChainofResponsibility.EuroinYenUmrechner;
import Winterprojekt.ChainofResponsibility.Factory.UmrechnerFactory;

public abstract class WR implements InewUmrechner {

    Ergebnis erg = new Ergebnis();
    AbstractUmrechner umrechnerkette;

    @Override
    public double umrechnen(String variante, double betrag) {
        if(umrechnerkette == null) {
            umrechnerkette = getUmrechnerkette();
        }

        //Wenn variante vonEuro ist führe diese Methode aus
        umrechneninEuro(umrechnerkette, variante, betrag, erg);
        return erg.getWert();
    }

    @Override
    public void umrechnerHinzufügen(String variante) {
        UmrechnerFactory uf = new UmrechnerFactory();
        AbstractUmrechner neuerumrechner = uf.neuerUmrechner(variante);

        if(neuerumrechner == null){
            System.out.println("Für diese Variante gibt es noch keinen Umrechner!");
        }else {

            if (umrechnerkette == null) {
                umrechnerkette = getUmrechnerkette();
            }

            //Setze den neuen Umrechner an den Anfang der Kette
            neuerumrechner.setNextUmrechner(umrechnerkette);
            umrechnerkette = neuerumrechner;
        }
    }

    //Diese Methoden werden in den Unteren Klassen ausgeführt bzw. implementiert
    abstract double umrechneninEuro(AbstractUmrechner umrechnerkette, String variante, double betrag, Ergebnis erg);

    //erstellen der Umrechnerchain
    //Soll ein Umrechner hinzugefügt oder gelöscht werden geschieht das Hier
    public static AbstractUmrechner getUmrechnerkette() {

        AbstractUmrechner EuroinDollar = new Logger(new EuroinDollarUmrechner("EuroinDollar", 1.144));
        AbstractUmrechner EuroinYen = new Logger(new Zusatzgebuehren(new EuroinYenUmrechner("EuroinYen", 126.14)));

        EuroinDollar.setNextUmrechner(EuroinYen);

        return EuroinDollar;
    }
}
