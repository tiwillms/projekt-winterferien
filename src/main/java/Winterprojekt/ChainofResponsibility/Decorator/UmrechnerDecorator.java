package Winterprojekt.ChainofResponsibility.Decorator;

import Winterprojekt.ChainofResponsibility.AbstractUmrechner;
import Winterprojekt.ChainofResponsibility.EuroinDollarUmrechner;

abstract class UmrechnerDecorator extends AbstractUmrechner {
    AbstractUmrechner tempeuroindollarUmrechner;

    public UmrechnerDecorator(AbstractUmrechner newUmrechner){
        tempeuroindollarUmrechner = newUmrechner;
    }

    public String getUmrechnervariante() {
        return tempeuroindollarUmrechner.getUmrechnervariante();
    }

    public double getFaktor() {
        return tempeuroindollarUmrechner.getFaktor();
    }

    @Override
    public double getErgebnisspeicher() {
        return super.getErgebnisspeicher();
    }

    @Override
    public double getBetragspeicher() {
        return super.getBetragspeicher();
    }
}
