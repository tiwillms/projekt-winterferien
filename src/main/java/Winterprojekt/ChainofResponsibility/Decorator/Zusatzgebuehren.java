package Winterprojekt.ChainofResponsibility.Decorator;

import Winterprojekt.ChainofResponsibility.AbstractUmrechner;

public class Zusatzgebuehren extends UmrechnerDecorator {
    public Zusatzgebuehren(AbstractUmrechner newUmrechner) {
        super(newUmrechner);
    }

    @Override
    public double getFaktor() {
        return super.getFaktor() * 0.9;
    }
}
