package Winterprojekt.ChainofResponsibility.Decorator;

import Winterprojekt.ChainofResponsibility.AbstractUmrechner;

public class Logger extends UmrechnerDecorator {

    public Logger(AbstractUmrechner newUmrechner) {
        super(newUmrechner);
    }

    @Override
    public String toString() {
        return "Umrechnungsvorgang, Variante " + getUmrechnervariante() + " mit dem Betrag " + getBetragspeicher() + " Euro. Ergebnis " + getErgebnisspeicher();
    }
}
