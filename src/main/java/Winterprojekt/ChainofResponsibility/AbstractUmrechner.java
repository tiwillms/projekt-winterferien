package Winterprojekt.ChainofResponsibility;

import Winterprojekt.IUmrechnen;
import Winterprojekt.WR;

/**
 * Die Klasse AbstractUmrechner implementiert die umrechnen Methode aus
 * Zur umrechnung wird eine Chain verwendet in der die Faktoren gespeichert sind
 */
public abstract class AbstractUmrechner {
    protected double faktor;
    protected String variante;
    private double ergebnisspeicher;
    private double betragspeicher;

    private AbstractUmrechner nextUmrechner;

    /**
     * Die Methode setNextUmrechner nimmt einen AbstractUmrechner entgegen
     * und setzt diesen als Nächsten umrechner
     * @param nextUmrechner
     */
    public void setNextUmrechner(AbstractUmrechner nextUmrechner) {

        this.nextUmrechner = nextUmrechner;
    }

    /**
     * Die Methode Löscht den umrechner aus der Kette mit der mitgegebenen Variante
     * @param variante
     */
    public void delUmrechner(String variante) {

        if(this.nextUmrechner.getUmrechnervariante().equals(variante)){
            this.nextUmrechner = null;
        }
    }

    public AbstractUmrechner getNextUmrechner() {
        return nextUmrechner;
    }

    public String getUmrechnervariante() {
        return variante;
    }

    public double getFaktor() {
        return faktor;
    }

    public double getErgebnisspeicher() {
        return ergebnisspeicher;
    }

    public void setErgebnisspeicher(double ergebnisspeicher) {
        this.ergebnisspeicher = ergebnisspeicher;
    }

    public double getBetragspeicher() {
        return betragspeicher;
    }

    public void setBetragspeicher(double betragspeicher) {
        this.betragspeicher = betragspeicher;
    }

    /**
     * Die Methode umrechnen ermittelt welcher KonkreteUmrechner
     * für die Umrechnung verantwortlich ist
     * und ruft dann die ergebnisausrechnen auf mit dem dementsprechenden Faktor
     * @param variante
     * @param betrag
     * @param ergebnis
     */
    public void umrechnen(String variante, double betrag, Ergebnis ergebnis) {
        //Den zu umwandelnden Betrag setzen (wird im Logger benötigt)
        setBetragspeicher(betrag);

        if(this.getUmrechnervariante().equals(variante)){
            //Berechnet das Ergebnis mit der Hilfsmethode und setzt den Wert der Klasse Ergebnis (callbyReferenz)
            ergebnis.setWert(this.ergebnisausrechnen(betrag));
            setErgebnisspeicher(ergebnis.getWert());

            //Ergebnis der Umrechnung ausgeben
            System.out.println(this.toString());
            //System.out.println("Es wurden: " + betrag + " Euro in " + ergebnis.getWert() + " " + getUmrechnervariante().substring(6) +" umgerechnet.");
        }else{
            if(this.nextUmrechner != null){
                //System.out.println("Wird an " + this.nextUmrechner + " weitergeleitet.");
                this.nextUmrechner.umrechnen(variante, betrag, ergebnis);
            }else{
                System.out.println("Umrechnungsvariante gibt es noch nicht!");
            }
        }
    }

    /**
     * Erechnet den Umgerechneten Wert mit den Angegebenen Faktor
     * aus dem konkreten Umrechner
     * @param betrag
     * @return ergebnis
     */
    public double ergebnisausrechnen(double betrag){

        double ergebnis = betrag * this.getFaktor();

        return ergebnis;
    }

    @Override
    public String toString() {
        return "";
        //("Umrechnungsvorgang, Variante " + getUmrechnervariante() + " mit dem Betrag " + getBetragspeicher() + " Euro. Ergebnis " + getErgebnisspeicher());
    }
}
