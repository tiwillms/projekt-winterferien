package Winterprojekt.ChainofResponsibility;

import Winterprojekt.ChainofResponsibility.Factory.UmrechnerFactory;

public class EuroinFranken extends AbstractUmrechner {
    public EuroinFranken(String variante, double faktor){
        this.variante = variante;
        this.faktor = faktor;
    }
}
