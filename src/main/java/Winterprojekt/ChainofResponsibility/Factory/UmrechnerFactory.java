package Winterprojekt.ChainofResponsibility.Factory;

import Winterprojekt.ChainofResponsibility.AbstractUmrechner;
import Winterprojekt.ChainofResponsibility.Decorator.Logger;
import Winterprojekt.ChainofResponsibility.EuroinFranken;
import Winterprojekt.ChainofResponsibility.EuroinPfund;

public class UmrechnerFactory {

    /**
     * Erstellt eine gewisse Alarmanlage nach der Anforderung Typ und gibt diese mit return zurück
     * @param variante
     * @return Alarmanlage
     */
    public AbstractUmrechner neuerUmrechner(String variante){
        if(variante.equals("EuroinPfund")){
            double faktor = 0.88;
            return new Logger(new EuroinPfund(variante, faktor));

        }else if(variante.equals("EuroinFranken")){
            double faktor = 1.13;
            return new Logger(new EuroinFranken(variante, faktor));
        }
        else{
            return null;
        }
    }
}
