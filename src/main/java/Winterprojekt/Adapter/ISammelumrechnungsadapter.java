package Winterprojekt.Adapter;

import Winterprojekt.ChainofResponsibility.Ergebnis;
import Winterprojekt.ISammelumrechnung;
import Winterprojekt.IUmrechnen;
import Winterprojekt.WR;

public class ISammelumrechnungsadapter implements ISammelumrechnung {
    IUmrechnen umrechner;

    public ISammelumrechnungsadapter(IUmrechnen newumrechner){
            this.umrechner = newumrechner;
    }

    @Override
    public double sammelumrechnung(double[] betraege, String variante) {
        double summeges = 0.0;

        //Aufsplitten des Arrays
        for(int i = 0; i < betraege.length; i++){
            double betrag = betraege[i];
            summeges = summeges + umrechner.umrechnen(variante, betrag);
            //System.out.println(summeges);
        }
        return summeges;
    }

}
